﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace cviceni8._4
{
    class Program
    {
        static void ZmenaVelikostiPismen (string vstupni, string vystupni)
        {
            int pismeno, pocet = 0, j = 0;
            char[] slovo = new char[30];
            StreamReader sr = new StreamReader(vstupni);
            StreamWriter sw = new StreamWriter(vystupni);
            FileInfo f = new FileInfo(vstupni);

            while (sr.Peek() != 13)
            {
                pismeno = (sr.Read());
                if (pismeno > 96 && pismeno < 123)
                    sw.Write((char)(pismeno - ' '));
                else
                    sw.Write((char)pismeno);
                pocet++;
            }

            for (int i = pocet; pocet < f.Length; i++)
            {
                if (sr.Peek() == 13)
                {
                    sr.Read();
                    sw.Write('\r');
                    continue;
                }
                else if (sr.Peek() == 10)
                {
                    sr.Read();
                    sw.Write('\n');
                    continue;
                }

                slovo[j] = (char)sr.Read();
                if (j < 29) j++;

                if (j > 3 && (slovo[j - 1] == 32 || sr.Peek() == -1) ) //if a else if podobne struktury
                {
                    slovo[0] -= ' ';
                    for (int k = 0; k < j; k++)
                        sw.Write(slovo[k]);
                    j = 0;
                    if (sr.Peek() == -1)
                        break;
                }
                else if (slovo[j - 1] == 32)
                {
                    for (int k = 0; k < j; k++)
                        sw.Write(slovo[k]);
                    j = 0;
                    if (sr.Peek() == -1)
                        break;
                }
            }

            sr.Close(); sw.Close();
        }

        static void Main(string[] args)
        {
            Stopwatch cas = new Stopwatch();
            long timeTicks, timeMs;

            cas.Start();
            ZmenaVelikostiPismen("aaa.txt", "novy.txt");
            cas.Stop();

            timeTicks = cas.ElapsedTicks;
            timeMs = cas.ElapsedMilliseconds;

            Console.WriteLine("{0} ticks, {1} ms", timeTicks, timeMs);
            Console.ReadLine();
        }
    }
}
