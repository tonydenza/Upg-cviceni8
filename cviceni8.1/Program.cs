﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace cviceni8._1
{
    class Program
    {
        static void NahodnySoubor(string soubor, int pocet)
        {
            BinaryWriter bw = new BinaryWriter(new FileStream(soubor, FileMode.Create));
            Random r = new Random();

            for (int i = 0; i < pocet; i++)
                bw.Write(r.Next(1, 1000));

            bw.Close();
        }

        static void Main(string[] args)
        {
            NahodnySoubor("cisla.dat", 1000);
        }
    }
}
