﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace cviceni8._2
{
    class Program
    {
        static void BinarniNaTextovy (string vstupni, string vystupni)
        {
            int cislo;

            BinaryReader br = new BinaryReader(new FileStream(vstupni, FileMode.Open));
            StreamWriter sw = new StreamWriter(vystupni);
            FileInfo f = new FileInfo(vstupni);
            for (int i = 1; i < f.Length/4 + 1; i++)
            {
                cislo = br.ReadInt32();

                if (i % 10 == 0)
                    sw.WriteLine(cislo);
                else
                {
                    sw.Write(cislo);
                    sw.Write(" ");
                }
            }

            br.Close(); sw.Close();
        }

        static void Main(string[] args)
        {
            BinarniNaTextovy("cisla.dat", "cisla.txt");
        }
    }
}
