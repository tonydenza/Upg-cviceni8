﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace cviceni8._3
{
    class Program
    {
        static void SoucetCisel (string vstupni, string vystupni)
        {
            string soucet = "";
            long soucetInt = 0;
            int pocet = 0;
            StreamReader sr = new StreamReader(vstupni);
            StreamWriter sw = new StreamWriter(vystupni);
            FileInfo f = new FileInfo(vstupni);

            for (int i = 1; i < f.Length + 1; i++)
            {
                if (sr.Peek() != 32 && sr.Peek() != 10 && sr.Peek() != 13)
                    soucet += (char) sr.Read();
                else
                {
                    if (sr.Peek() == 10)
                    {
                        sr.Read();
                        continue;
                    }
                    for (int j = soucet.Length - 1, index = 1; j >= 0; j--, index *= 10)
                        soucetInt += ((char) soucet[j] - '0') * index;
                    pocet++;
                    soucet = "";
                    if (sr.Peek() == 32 || sr.Peek() == 13)
                        sr.Read();
                }

                if (pocet % 10 == 0 && pocet != 0 && soucet == "")
                {
                    sw.WriteLine(soucetInt);
                    soucetInt = 0;
                }
            }

            sr.Close(); sw.Close();
        }

        static void Main(string[] args)
        {
            SoucetCisel("cisla.txt", "soucet.txt");
        }
    }
}
